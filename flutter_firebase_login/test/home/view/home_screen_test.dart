import 'package:flutter/material.dart';
import 'package:flutter_firebase_login/home/view/home_screen.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('displays static text', (WidgetTester tester) async {
    // Render the widget.
    await tester.pumpWidget(MaterialApp(
      home: StaticHomeScreenTexts(),
    ));
    // Let the snapshots stream fire a snapshot.
    await tester.idle();
    // Re-render.
    await tester.pump();
    // // Verify the output.
    expect(
        find.text("Welcome to the deX-cards app, your best studying assistant.",
            skipOffstage: false),
        findsWidgets);
  });
}
