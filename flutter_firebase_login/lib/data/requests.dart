import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;
import 'package:cloud_firestore/cloud_firestore.dart';

import '../home/widgets/my_card.dart';
import '../home/widgets/my_stack.dart';

class Requests {
  static FirebaseFirestore firestore = FirebaseFirestore.instance;
  static var firebaseAuth = firebase_auth.FirebaseAuth.instance;
  static CollectionReference stacks = firestore.collection('stacks');

  static void deleteCard(FirebaseFirestore fs, int index, MyStack stack) async {
    var cards = [];

    try {
      await fs
          .collection('stacks')
          .doc(stack.id)
          .get()
          .then((value) => value.data()!['cards'])
          .then((value) {
        cards = value;
      });
    } catch (e) {
      cards = [];
    }

    cards.removeAt(index);

    Requests.stacks.doc(stack.id).update({'cards': cards});
  }

  static void saveCard(FirebaseFirestore fs, MyCard card) async {
    var cards = [];

    try {
      await fs
          .collection('stacks')
          .doc(card.stack.id)
          .get()
          .then((value) => value.data()!['cards'])
          .then((value) {
        cards = value;
      });
    } catch (e) {
      cards = [];
    }

    cards.add({'question': card.question, 'answer': card.answer});

    Requests.stacks.doc(card.stack.id).update({'cards': cards});
    card.stack.cards.add({'question': card.question, 'answer': card.answer});
  }

  static void favouriteCard(FirebaseFirestore fs, MyCard card) async {
    List cards = [];

    try {
      await fs
          .collection('users')
          .doc(firebaseAuth.currentUser!.uid)
          .get()
          .then((value) => value.data()!['favourites'])
          .then((value) {
        cards = value;
      });
    } catch (e) {
      cards = [];
    }

    if (!await isFav(fs, card.question)) {
      cards.add({
        'answer': card.answer,
        'question': card.question,
      });
    }
    firestore
        .collection('users')
        .doc(firebaseAuth.currentUser!.uid)
        .set({'favourites': cards});
  }

  static Future<bool> isFav(FirebaseFirestore fs, String question) async {
    List cards = [];

    try {
      await fs
          .collection('users')
          .doc(firebaseAuth.currentUser!.uid)
          .get()
          .then((value) => value.data()!['favourites'])
          .then((value) {
        cards = value;
      });
    } catch (e) {
      cards = [];
    }

    bool _isFav = false;

    for (var i = 0; i < cards.length; i++) {
      if (cards[i]['question'] == question) _isFav = true;
    }
    return _isFav;
  }

  static void deleteStack(String id) {
    stacks.doc(id).delete();
  }

  static MyStack saveStack(
      String id, String prof, String sub, String rootWidget) {
    //TODO check whether stack is already created
    DocumentReference docReference = stacks.doc(id);

    docReference
        .set({'id': id, 'prof': prof, 'sub': sub})
        .then((value) => print("Stack Added"))
        .catchError((error) => print("Failed to add stack: $error"));

    return MyStack(
      id: id,
      cards: [],
      prof: prof,
      sub: sub,
      reference: docReference,
      rootWidget: rootWidget,
    );
  }
}
