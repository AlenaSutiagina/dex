import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_firebase_login/home/view/stack_page.dart';

class MyStack extends StatelessWidget {
  MyStack({
    required this.id,
    required this.reference,
    required this.cards,
    required this.sub,
    required this.prof,
    required this.rootWidget,
  }) : super();
  final String id;
  final DocumentReference reference;
  final List cards;
  final String prof;
  final String sub;
  final String rootWidget;
  @override
  Widget build(BuildContext context) {
    return StackPage(stack: this);
  }
}
