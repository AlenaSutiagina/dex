import 'package:flutter/material.dart';

class PurpleIcon extends Icon {
  PurpleIcon(IconData? icon, {required this.size}) : super(icon);
  final double size;
  @override
  Widget build(BuildContext context) {
    return Icon(
      icon,
      size: size,
      color: Colors.purple[100],
    );
  }
}

class PurpleSizedIcon extends Icon {
  PurpleSizedIcon(IconData? icon) : super(icon);

  @override
  Widget build(BuildContext context) {
    return Icon(
      icon,
      size: 32,
      color: Colors.purple[100],
    );
  }
}
