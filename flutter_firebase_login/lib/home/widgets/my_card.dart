import 'package:flutter/material.dart';
import 'package:flutter_firebase_login/home/view/ccard_screen.dart';
import 'package:flutter_firebase_login/home/view/stack_page.dart';
import 'package:flutter_firebase_login/home/widgets/my_stack.dart';

import '../../data/requests.dart';
import 'custom_icon.dart';

class MyCard extends StatelessWidget {
  MyCard({
    required this.stack,
    required this.question,
    required this.answer,
    required this.index,
  }) : super();
  final MyStack stack;
  final String question;
  final String answer;
  final int index;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Color(0xFFE1BEE7)),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (contex) => StackPage(stack: this.stack)));
            },
          ),
          title: const Text("deX",
              style: TextStyle(fontSize: 28, color: Color(0xFFE1BEE7))),
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0.0,
        ),
        body: Align(
            alignment: const Alignment(0, -0.9),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              //  mainAxisAlignment: MainAxisAlignment.center,
              //  crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Align(
                      alignment: Alignment(1.85, 0.6),
                      child: IconButton(
                        alignment: Alignment.topRight,
                        icon: PurpleSizedIcon(Icons.delete_forever_rounded),
                        tooltip: 'Delete card',
                        onPressed: () {
                          Requests.deleteCard(
                              Requests.firestore, this.index, stack);
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    SizedBox(width: 20),
                    Align(
                      alignment: Alignment(2.85, 0.6),
                      child: IconButton(
                        alignment: Alignment.topRight,
                        icon: PurpleSizedIcon(Icons.add_to_photos_rounded),
                        tooltip: 'Create new card',
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (contex) =>
                                      CreateCardScreen(stack: stack)));
                        },
                      ),
                    ),
                    SizedBox(width: 40),
                  ],
                ),
                Column(
                  children: [
                    const SizedBox(height: 30),
                    Container(
                      padding: const EdgeInsets.all(5.0),
                      margin: const EdgeInsets.all(10.0),
                      height: 130,
                      width: 500,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.purple)),
                      child: Column(
                        children: <Widget>[
                          Text(this.question,
                              style: TextStyle(fontSize: 24),
                              textAlign: TextAlign.center),
                          const SizedBox(height: 8),
                        ],
                      ),
                    ),
                    SizedBox(height: 30),
                    Container(
                      padding: const EdgeInsets.all(5.0),
                      margin: const EdgeInsets.all(10.0),
                      height: 230,
                      width: 500,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.purple)),
                      child: Column(
                        children: <Widget>[
                          Text(this.answer,
                              style: TextStyle(fontSize: 24),
                              textAlign: TextAlign.center),
                        ],
                      ),
                    ),
                    const SizedBox(height: 40),
                  ],
                ),
              ],
            )));
  }
}
