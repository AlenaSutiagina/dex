import 'package:flutter/material.dart';
import 'package:flutter_firebase_login/home/view/stack_page.dart';

import '../widgets/my_stack.dart';

class Result extends StatelessWidget {
  Result(
      {required this.stack,
      required this.correctAnswers,
      required this.allQuestions})
      : super();
  final int correctAnswers;
  final int allQuestions;
  final MyStack stack;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Color(0xFFE1BEE7)),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (contex) => StackPage(stack: stack)));
            },
          ),
          title: const Text("deX",
              style: TextStyle(fontSize: 28, color: Color(0xFFE1BEE7))),
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0.0,
        ),
        body: Align(
            alignment: const Alignment(0, -0.9),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Column(
                  children: [
                    const SizedBox(height: 30),
                    Text("Congratulations!",
                        style: TextStyle(fontSize: 35),
                        textAlign: TextAlign.center),
                    const SizedBox(height: 80),
                    Text("You have finished the \"" + stack.sub + "\" stack",
                        style: TextStyle(fontSize: 26),
                        textAlign: TextAlign.center),
                    const SizedBox(height: 40),
                    Text("Number of correct answers:",
                        style: TextStyle(fontSize: 26),
                        textAlign: TextAlign.center),
                    const SizedBox(height: 50),
                    Text(
                        correctAnswers.toString() +
                            "/" +
                            allQuestions.toString(),
                        style: TextStyle(fontSize: 35),
                        textAlign: TextAlign.center),
                    const SizedBox(height: 120),
                    TextButton(
                        style: ButtonStyle(
                          foregroundColor: MaterialStateProperty.all<Color>(
                              Color(0xFFE1BEE7)),
                        ),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (contex) =>
                                      StackPage(stack: stack)));
                        },
                        child: Text(
                          'Back to stack',
                          style: TextStyle(fontSize: 20),
                        )),
                    const SizedBox(height: 40),
                  ],
                ),
              ],
            )));
  }
}
