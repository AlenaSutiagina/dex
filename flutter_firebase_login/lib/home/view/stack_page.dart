import 'package:flutter/material.dart';
import 'package:flutter_firebase_login/data/requests.dart';
import 'package:flutter_firebase_login/home/home.dart';
import 'package:flutter_firebase_login/home/view/card_learn_mode.dart';
import 'package:flutter_firebase_login/home/view/ccard_screen.dart';
import 'package:flutter_firebase_login/home/widgets/my_stack.dart';

import '../widgets/custom_icon.dart';
import 'card_page.dart';

class StackPage extends StatefulWidget {
  final MyStack stack;

  const StackPage({Key? key, required this.stack}) : super(key: key);

  @override
  State<StackPage> createState() => _StackPageState();
}

class _StackPageState extends State<StackPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Color(0xFFE1BEE7)),
            onPressed: () {
              if (widget.stack.rootWidget == "HomeScreen") {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (contex) => HomePage(
                              index: 0,
                            )));
              } else if (widget.stack.rootWidget == "AllStackScreen") {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (contex) => HomePage(
                              index: 3,
                            )));
              }
            },
          ),
          title: const Text("deX",
              style: TextStyle(fontSize: 28, color: Color(0xFFE1BEE7))),
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0.0),
      body: Align(
        alignment: const Alignment(0, -0.9),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const SizedBox(height: 12),
            Text(widget.stack.sub + ", " + widget.stack.prof,
                style: TextStyle(fontSize: 24)),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Align(
                  alignment: Alignment.center,
                  child: TextButton(
                      style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all<Color>(Color(0xFFE1BEE7)),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MyCardLearnMode(
                                      index: 0,
                                      stack: widget.stack,
                                    )));
                      },
                      child: Text(
                        'Learning mode',
                        style: TextStyle(fontSize: 20),
                      ))),
              SizedBox(width: 130),
              Align(
                alignment: Alignment.topRight,
                child: IconButton(
                  alignment: Alignment.topRight,
                  icon: PurpleSizedIcon(Icons.add_to_photos_rounded),
                  tooltip: 'Create card',
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (contex) =>
                                CreateCardScreen(stack: widget.stack)));
                  },
                ),
              ),
              IconButton(
                  onPressed: () {
                    Requests.deleteStack(widget.stack.id);
                    Navigator.pop(context);
                  },
                  icon: PurpleSizedIcon(Icons.delete_forever_rounded))
            ]),
            const SizedBox(height: 12),
            widget.stack.cards.length != 0
                ? Expanded(
                    child: ListView.builder(
                      itemCount: widget.stack.cards.length,
                      itemBuilder: (context, index) {
                        return Container(
                            padding: const EdgeInsets.all(5.0),
                            margin: const EdgeInsets.all(10.0),
                            height: 130,
                            width: 500,
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.purple)),
                            child: Column(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment(0.85, 0.6),
                                  child: SizedBox(
                                    height: 22.0,
                                    width: 22.0,
                                    child: IconButton(
                                      alignment: Alignment.topRight,
                                      icon: PurpleIcon(Icons.add_to_home_screen,
                                          size: 27),
                                      tooltip: 'Go to card',
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => CardPage(
                                                    stack: widget.stack,
                                                    index: index)));
                                      },
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 18),
                                Text(widget.stack.cards[index]['question'],
                                    style: TextStyle(fontSize: 24),
                                    textAlign: TextAlign.center),
                                const SizedBox(height: 8),
                              ],
                            ));
                      },
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
