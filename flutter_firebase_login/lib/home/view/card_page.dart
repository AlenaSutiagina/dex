import 'package:flutter/material.dart';
import 'package:flutter_firebase_login/data/requests.dart';
import 'package:flutter_firebase_login/home/widgets/custom_icon.dart';
import 'package:flutter_firebase_login/home/widgets/my_stack.dart';

import '../widgets/my_card.dart';

class CardPage extends StatefulWidget {
  CardPage({
    Key? key,
    required this.stack,
    required this.index,
  });
  final MyStack stack;
  int index;
  @override
  State<StatefulWidget> createState() => CardPageState();
}

class CardPageState extends State<CardPage> {
  bool _isFav = false;

  Future<bool> _isFavourite() async {
    return await Requests.isFav(
        Requests.firestore, widget.stack.cards[widget.index]['question']);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MyCard(
        stack: widget.stack,
        answer: widget.stack.cards[widget.index]['answer'],
        question: widget.stack.cards[widget.index]['question'],
        index: widget.index,
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        child: Container(
          height: 75,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(
                  onPressed: () {
                    setState(() {
                      if (widget.index > 0) {
                        widget.index -= 1;
                        _isFav = false;
                      }
                    });
                  },
                  icon: PurpleSizedIcon(Icons.arrow_back_ios)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      _isFav = !_isFav;
                    });
                    Requests.favouriteCard(
                      Requests.firestore,
                      MyCard(
                        stack: widget.stack,
                        answer: widget.stack.cards[widget.index]['answer'],
                        question: widget.stack.cards[widget.index]['question'],
                        index: widget.index,
                      ),
                    );
                  },
                  icon: _isFav
                      ? PurpleSizedIcon(Icons.star)
                      : PurpleSizedIcon(Icons.star_border)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      if (widget.index < widget.stack.cards.length - 1) {
                        widget.index += 1;
                        _isFav = false;
                      }
                    });
                  },
                  icon: PurpleSizedIcon(Icons.arrow_forward_ios))
            ],
          ),
        ),
      ),
    );
  }
}
