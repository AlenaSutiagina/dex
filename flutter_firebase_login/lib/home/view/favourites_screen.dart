import 'package:flutter/material.dart';
import 'package:flutter_firebase_login/data/requests.dart';
import 'package:flutter_firebase_login/home/widgets/custom_icon.dart';

/* import 'package:flutter_settings_screens/flutter_settings_screens.dart';
 */
class FavouritesScreen extends StatefulWidget {
  FavouritesScreen({Key? key}) : super(key: key);
  List cards = [];

  @override
  State<FavouritesScreen> createState() => _FavouritesScreenState();
}

class _FavouritesScreenState extends State<FavouritesScreen> {
  void getData() async {
    List cards = [];

    try {
      await Requests.firestore
          .collection('users')
          .doc(Requests.firebaseAuth.currentUser!.uid)
          .get()
          .then((value) => value.data()!['favourites'])
          .then((value) {
        cards = value;
      });
    } catch (e) {
      cards = [];
    }
    setState(() {
      widget.cards = cards;
    });
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: widget.cards.length != 0
          ? ListView.builder(
              itemCount: widget.cards.length,
              itemBuilder: (context, index) {
                return Container(
                    padding: const EdgeInsets.all(5.0),
                    margin: const EdgeInsets.all(10.0),
                    height: 130,
                    width: 500,
                    decoration:
                        BoxDecoration(border: Border.all(color: Colors.purple)),
                    child: Column(
                      children: <Widget>[
                        Align(
                          alignment: Alignment(0.85, 0.6),
                          child: SizedBox(
                            height: 22.0,
                            width: 22.0,
                            child: IconButton(
                              alignment: Alignment.topRight,
                              icon: PurpleIcon(Icons.add_to_home_screen,
                                  size: 27),
                              tooltip: 'Go to card',
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => FavouriteCardPage(
                                              index: index,
                                              cards: widget.cards,
                                            )));
                              },
                            ),
                          ),
                        ),
                        const SizedBox(height: 18),
                        Text(widget.cards[index]['question'],
                            style: TextStyle(fontSize: 24),
                            textAlign: TextAlign.center),
                        const SizedBox(height: 8),
                      ],
                    ));
              },
            )
          : Container(),
    );
  }
}

class FavouriteCardPage extends StatefulWidget {
  FavouriteCardPage({
    Key? key,
    required this.index,
    required this.cards,
  });
  int index;
  List cards;

  @override
  State<StatefulWidget> createState() => _FavouriteCardPageState();
}

class _FavouriteCardPageState extends State<FavouriteCardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Color(0xFFE1BEE7)),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: const Text("deX",
            style: TextStyle(fontSize: 28, color: Color(0xFFE1BEE7))),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.0,
      ),
      body: Align(
        alignment: const Alignment(0, -0.9),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          //  mainAxisAlignment: MainAxisAlignment.center,
          //  crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Column(
              children: [
                const SizedBox(height: 30),
                Container(
                  padding: const EdgeInsets.all(5.0),
                  margin: const EdgeInsets.all(10.0),
                  height: 130,
                  width: 500,
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.purple)),
                  child: Column(
                    children: <Widget>[
                      Text(widget.cards[widget.index]['question'],
                          style: TextStyle(fontSize: 24),
                          textAlign: TextAlign.center),
                      const SizedBox(height: 8),
                    ],
                  ),
                ),
                SizedBox(height: 30),
                Container(
                  padding: const EdgeInsets.all(5.0),
                  margin: const EdgeInsets.all(10.0),
                  height: 230,
                  width: 500,
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.purple)),
                  child: Column(
                    children: <Widget>[
                      Text(widget.cards[widget.index]['answer'],
                          style: TextStyle(fontSize: 24),
                          textAlign: TextAlign.center),
                    ],
                  ),
                ),
                const SizedBox(height: 40),
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        child: Container(
          height: 75,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(
                  onPressed: () {
                    setState(() {
                      if (widget.index > 0) {
                        widget.index -= 1;
                      }
                    });
                  },
                  icon: PurpleSizedIcon(Icons.arrow_back_ios)),
              IconButton(onPressed: () {}, icon: PurpleSizedIcon(Icons.star)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      if (widget.index < widget.cards.length - 1) {
                        widget.index += 1;
                      }
                    });
                  },
                  icon: PurpleSizedIcon(Icons.arrow_forward_ios))
            ],
          ),
        ),
      ),
    );
  }
}
