import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_firebase_login/home/view/stack_page.dart';
import 'package:flutter_firebase_login/home/widgets/my_stack.dart';

import '../widgets/custom_icon.dart';
import 'learning_mode_result.dart';

class MyCardLearnMode extends StatefulWidget {
  MyCardLearnMode({
    required this.index,
    required this.stack,
  }) : super();
  int index;
  final MyStack stack;
  @override
  State<MyCardLearnMode> createState() => _MyCardLearnModeState();
}

class _MyCardLearnModeState extends State<MyCardLearnMode> {
  int _selectedIndex = 0;
  int correctAnswers = 0;
  var allCorrectAnswers = [];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _answers = <Widget>[
      ImageFiltered(
          imageFilter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          child: Text(widget.stack.cards[widget.index]['answer'],
              style: TextStyle(fontSize: 24))),
      Text(widget.stack.cards[widget.index]['answer'],
          style: TextStyle(fontSize: 24), textAlign: TextAlign.center)
    ];
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Color(0xFFE1BEE7)),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (contex) => StackPage(stack: widget.stack)));
            },
          ),
          title: const Text("deX",
              style: TextStyle(fontSize: 28, color: Color(0xFFE1BEE7))),
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0.0,
        ),
        body: Align(
            alignment: const Alignment(0, -0.9),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Column(
                  children: [
                    const SizedBox(height: 30),
                    Container(
                      padding: const EdgeInsets.all(5.0),
                      margin: const EdgeInsets.all(10.0),
                      height: 130,
                      width: 500,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.purple)),
                      child: Column(
                        children: <Widget>[
                          Text(widget.stack.cards[widget.index]['question'],
                              style: TextStyle(fontSize: 24),
                              textAlign: TextAlign.center),
                          const SizedBox(height: 8),
                        ],
                      ),
                    ),
                    SizedBox(height: 30),
                    Container(
                      padding: const EdgeInsets.all(5.0),
                      margin: const EdgeInsets.all(10.0),
                      height: 230,
                      width: 500,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.purple),
                      ),
                      child: Column(
                        children: <Widget>[_answers.elementAt(_selectedIndex)],
                      ),
                    ),
                    const SizedBox(height: 40),
                  ],
                ),
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  _selectedIndex == 1
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Align(
                              child: IconButton(
                                alignment: Alignment.bottomLeft,
                                icon: PurpleSizedIcon(Icons.arrow_back_ios),
                                tooltip: 'Previous card',
                                onPressed: () {
                                  setState(() {
                                    if (widget.index > 0) {
                                      widget.index -= 1;
                                      _selectedIndex = 0;
                                    }
                                  });
                                },
                              ),
                            ),
                            SizedBox(width: 20),
                            Align(
                              child: IconButton(
                                alignment: Alignment.bottomCenter,
                                icon: PurpleSizedIcon(Icons.check),
                                tooltip: 'I answered correctly',
                                onPressed: () {
                                  if (!allCorrectAnswers
                                      .contains(widget.index)) {
                                    allCorrectAnswers.add(widget.index);
                                    correctAnswers++;
                                  }
                                },
                              ),
                            ),
                            SizedBox(width: 20),
                            Align(
                              child: IconButton(
                                alignment: Alignment.bottomRight,
                                icon: PurpleSizedIcon(Icons.arrow_forward_ios),
                                tooltip: 'Next card',
                                onPressed: () {
                                  setState(() {
                                    if (widget.index <
                                        widget.stack.cards.length - 1) {
                                      widget.index += 1;
                                      _selectedIndex = 0;
                                    } else {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => Result(
                                                    stack: widget.stack,
                                                    correctAnswers:
                                                        correctAnswers,
                                                    allQuestions: widget
                                                        .stack.cards.length,
                                                  )));
                                    }
                                  });
                                },
                              ),
                            ),
                          ],
                        )
                      : Align(
                          child: IconButton(
                            alignment: Alignment.center,
                            icon:
                                PurpleSizedIcon(Icons.remove_red_eye_outlined),
                            tooltip: 'Show answer',
                            onPressed: () => {_onItemTapped(1)},
                          ),
                        ),
                ])
              ],
            )));
  }
}
