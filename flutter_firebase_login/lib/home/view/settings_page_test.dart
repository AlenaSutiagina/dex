import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_firebase_login/data/requests.dart';
import 'package:flutter_settings_screens/flutter_settings_screens.dart';

import '../../app/bloc/app_bloc.dart';

class MySettings extends StatefulWidget {
  const MySettings({Key? key}) : super(key: key);

  @override
  State<MySettings> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MySettings> {
  final user = FirebaseAuth.instance.currentUser!;

  @override
  Widget build(BuildContext context) {
    final user = FirebaseAuth.instance.currentUser!;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Color(0xFFE1BEE7)),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: const Text("deX",
            style: TextStyle(fontSize: 28, color: Color(0xFFE1BEE7))),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.0,
      ),
      body: Column(children: [
        SettingsGroup(
          title: "Account",
          children: <Widget>[
            ListTile(
              leading: getIcon(),
              title: Text(user.email!),
            ),
            logoutButton(),
            deleteProfileButton(),
          ],
        ),
        SettingsGroup(title: "Settings", children: <Widget>[])
      ]),
    );
  }

  ListTile logoutButton() {
    return ListTile(
      leading: Icon(
        Icons.logout,
        size: 40,
      ),
      title: const Text("Logout"),
      onTap: () {
        Navigator.pop(context);
        context.read<AppBloc>().add(AppLogoutRequested());
      },
    );
  }

  ListTile deleteProfileButton() {
    return ListTile(
      leading: Icon(
        Icons.no_accounts,
        size: 40,
      ),
      title: const Text("Delete profile"),
      onTap: () {
        _displayTextInputDialog(context);
      },
    );
  }

  TextEditingController _textFieldController = TextEditingController();
  String valueText = "";

  Future<void> _displayTextInputDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('TextField in Dialog'),
            content: TextField(
              onChanged: (value) {
                setState(() {
                  valueText = value;
                });
              },
              controller: _textFieldController,
              decoration: InputDecoration(hintText: "Enter your password"),
            ),
            actions: <Widget>[
              TextButton(
                child: Text('CANCEL'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              ElevatedButton(
                child: Text('OK'),
                onPressed: () async {
                  Navigator.pop(context);
                  user.reauthenticateWithCredential(
                      EmailAuthProvider.credential(
                          email: user.email!, password: valueText));
                  Requests.firestore.collection("users").doc(user.uid).delete();
                  try {
                    await user.delete();
                  } catch (e) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                            "Could not delete user, log back in and try again.")));
                    return;
                  }
                  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text("Deleted user successfully")));
                },
              ),
            ],
          );
        });
  }

  getIcon() {
    if (user.providerData[0].providerId == 'google.com') {
      return CircleAvatar(
        backgroundImage: NetworkImage(user.photoURL!),
      );
    } else {
      return const Icon(
        Icons.account_circle,
        size: 40,
      );
    }
  }
}
